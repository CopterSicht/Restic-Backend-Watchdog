# Restic-Backend-Watchdog

Watchdog for restic backup that are automatic triggered via an interval

**TLDR:** Bash script that emails you if your restic backup interval is overdue.

![Screenshot of email](https://git.mosad.xyz/CopterSicht/Restic-Backend-Watchdog/raw/branch/master/Screenshot_20200425_112352.png)

## How does it work?
The script monitors the backend of a restic backup. The bash script is executed via cronjob or a systemd-timer periodically. The user sets an interval for each restic repo (e.g. your laptop-backup). This is the same interval as the one that triggers the automatic backup from your device. If your automatic backup failed (e.g. no Internet), there will be no changes in den repo. The script will notice that and emails you.

## Features
* runs just with bash
* no restic authentication needed
* multiple repos
* individual interval for each repo
* custom email server via [ssmtp](https://wiki.debian.org/sSMTP)
* loud alarm buzzer via [FTDI_simple_alarm](https://git.mosad.xyz/localhorst/FTDI_simple_alarm)

## Planned Features
* Able to determine a failed restic repo task from a successful task

## Installation
#### 1. download the check_AutoBackup.sh
#### 2. chmod +x check_AutoBackup.sh
#### 3. install ssmtp
#### 4. nano /etc/ssmtp/ssmtp.conf

```UseSTARTTLS=YES
FromLineOverride=YES
root=
mailhub=mail.yourmailserver.tld:587
AuthUser=mailuser@maildomain.tld
AuthPass=yourmailuserpassword
```
#### 5. nano /etc/ssmtp/revaliases 
```
root:mailuser@maildomain.tld:mail.yourmailserver.tld:587
```

#### 6. edit settings in check_AutoBackup.sh

```
DEBUG="true" #true --> dont send email
timestampFolder="index" #folder for reading timestamp
backupFolder="./backups" #directory that contains all repos

receiverEmail="mailuser@maildomain.tld"
senderEmail="mailuser@maildomain.tld"
senderName="AutoBackup Watchdog"
```

#### 7. edit repo settings in check_AutoBackup.sh

```
repos_name[0]=" first backup repo"
repos_directory[0]="first_repo" #repo in backupFolder
repos_interval[0]=21600 #sec
repos_interval_tolerance[0]=10 #%
```
#### 8. install crontab or systemd-timer

#### 9. optional install FTDI simple alarm

[FTDI_simple_alarm](https://git.mosad.xyz/localhorst/FTDI_simple_alarm)

uncomment line 117 in check_AutoBackup.sh