#! /bin/bash

### GENERAL SETTINGS ###
DEBUG="true"
timestampFolder="index" #folder for reading timestamp
backupFolder="./backups" #directory that contains all repos
########################
###     eMail        ###
receiverEmail="admin@coptersicht.de"
senderEmail="autobackupwatchdog@coptersicht.de"
senderName="AutoBackup Watchdog"
########################

######### REPO SETTINGS #########

repos_name[0]=" first backup repo"
repos_directory[0]="first_repo"
repos_interval[0]=21600 #sec
repos_interval_tolerance[0]=10 #%

#repos_name[1]="second backup repo"
#repos_directory[1]="second_repo"
#repos_interval[1]=21600 #sec
#repos_interval_tolerance[1]=10 #%

#repos_name[2]=" another backup repo"
#repos_directory[2]="another_repo"
#repos_interval[2]=21600 #sec
#repos_interval_tolerance[2]=10 #%
#################################

function fn_convertSecondsToHumanReadable() {
fn_result="$(echo $(($1/86400))d $(($(($1 - $1/86400*86400))/3600))h:$(($(($1 - $1/86400*86400))%3600/60))m:$(($(($1 - $1/86400*86400))%60))s)"
}

echo "Starting AutoBackup Watchdog"
echo "Local time: $(date)"

cd $backupFolder #jump to backup folder that contains the repos

repo_count=$(("${#repos_name[@]}"))
echo "Repo Count: $repo_count"
repo_count=$(($repo_count-1))

for i in $(eval echo "{0..$repo_count}") #loop through all repos
do
    echo " "
    echo "Checking repo: ${repos_name[i]}"

    if [ -d "${repos_directory[i]}" ]; then
        # Take action if ${repos_directory[i]} exists ###
        cd ${repos_directory[i]}   #jump into repo
    else
        # Repo doesnt exist
        echo "Repo ${repos_directory[i]} doesnt exist!"
                rm -f mail_content.txt
                cat >> mail_content.txt <<EOL
Subject: AutoBackup Failure "${repos_name[i]}"
From: $senderEmail
Content-Type: text/html; charset="utf8
<head>
    <style>
        h1{
            text-align: center;
            font-size: 60; 
            color: red;
            margin: 30px;
            }
        h2{
            font-size: 30; 
            color: black;
            }
        div {
            margin: 30px;
            font-family: "Lucida Console", Courier, monospace;
            }
        .overdue{
            color: red;
            }
        footer{
            margin-top: 9em;
            }
    </style>
</head>
<body>
    <h1>AutoBackup Failure</h1>  
    <h2>Directory for the automatic backup for "${repos_name[i]}" not found</h2>  
    <div>
        <p>Local time: $(date)</p>
        <p>Failed backup repo: ${repos_name[i]}</p>
    </div>
</body>
<footer>
    <p>Version: 0.2</p>
    <p>Author: <a href="mailto:hendrik.schutter@coptersicht.de">hendrik.schutter@coptersicht.de</a></p>
</footer>

EOL
        if [ "$DEBUG" = "false" ];
        then 
            ssmtp -f$senderEmail -F$senderName $receiverEmail < mail_content.txt
            rm mail_content.txt
            #/etc/init.d/alarm start
        fi
    fi
    timestamp=$(date -r "$timestampFolder" +%s) #get timestap of last backup aka the "index" folder
    time=$(date +%s) #get local time
    diff=$(($time-$timestamp)) #compute diff time
    lastBackup=$(date -d @$timestamp) #convert unix time stamp in human readable
    tolerance=$(( ${repos_interval[i]}*${repos_interval_tolerance[i]}/100)) #compute tolerance based on percentage
    total_interval=$((${repos_interval[i]}+$tolerance)) #compute total interval based on (interval+tolerance)
    cd .. #leave repo
    echo "Last backup: $lastBackup"

    if [ "$diff" -gt "$total_interval" ]; then #test if last backup is overdue
        overdue=$(($diff-${repos_interval[i]})) #stores diff time since last backup in seconds
        fn_convertSecondsToHumanReadable $overdue # convert in human readable
        overdue=$fn_result #stores diff time since last backup in human readable
        fn_convertSecondsToHumanReadable ${repos_interval[i]} #convert planned interval for repo in human readable
        plannedIntervall=$fn_result #stores the planned interval for repo in human readable
        echo -e "\e[31mOverdue: $overdue"
        echo -e "\e[39m " #reset console color to black
        rm -f mail_content.txt
        cat >> mail_content.txt <<EOL
Subject: AutoBackup Failure "${repos_name[i]}"
From: $senderEmail
Content-Type: text/html; charset="utf8
<head>
    <style>
        h1{
            text-align: center;
            font-size: 60; 
            color: red;
            margin: 30px;
            }
        h2{
            font-size: 30; 
            color: black;
            }
        div {
            margin: 30px;
            font-family: "Lucida Console", Courier, monospace;
            }
        .overdue{
            color: red;
            }
        footer{
            margin-top: 9em;
            }
    </style>
</head>
<body>
    <h1>AutoBackup Failure</h1>  
    <h2>Overdue for the automatic backup for "${repos_name[i]}"</h2>  
    <div>
        <p>Local time: $(date)</p>
        <p>Failed backup repo: ${repos_name[i]}</p>
        <p>Planned backup interval: $plannedIntervall</p>
        <p>Last backup: $lastBackup</p>
        <p>Overdue: <a class="overdue">$overdue</a></p>
    </div>
</body>
<footer>
    <p>Version: 0.2</p>
    <p>Author: <a href="mailto:hendrik.schutter@coptersicht.de">hendrik.schutter@coptersicht.de</a></p>
</footer>

EOL
        if [ "$DEBUG" = "false" ];
        then 
            ssmtp -f$senderEmail -F$senderName $receiverEmail < mail_content.txt
            rm mail_content.txt
            #/etc/init.d/alarm start
        fi
    fi
done
echo " "
echo "Finished"
